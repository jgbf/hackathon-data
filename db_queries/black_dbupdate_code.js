// Copy the weather data into the relevant point data
var c = f = 0;
db.getCollection("weather_api_data").find().limit(1).forEach(function (data) {
    if (!(++c % 100)) {
        print(c + "/" + f);
    }
    var point = db.getCollection("relevant_points").findOne({ _id: data.external_id });
    if (!point) {
        f++;
        return;
    }
    data.hourly.forEach(function (hourlyData) {
        db.getCollection("relevant_points2").insertOne({
            external_id: point._id,
            latitude: point.latitude,
            longitude: point.longitude,
            dt: hourlyData.dt,
            date: new Date(hourlyData.dt * 1000),
            temp: hourlyData.temp,
            feels_like: hourlyData.feels_like,
            pressure: hourlyData.pressure,
            humidity: hourlyData.humidity,
            dew_point: hourlyData.dew_point,
            clouds: hourlyData.clouds,
            visibility: hourlyData.visibility,
            wind_speed: hourlyData.wind_speed,
            wind_deg: hourlyData.wind_deg,
            weather: hourlyData.weather
        });
    });
});
// Update the confidence for every relevant point
db.getCollection("relevant_points2").update({}, {$set: { confidence: 0 }}, { multi: true });
// Initialize the assigned_to property for all fire alerts
db.getCollection("modis_c6_usa_7d").update({}, {$set: { assigned_to: 0 }}, { multi: true });
// Get the closest fire alert from the modis collection, and join it to the relevant point collection
function calcCrow(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    return d;
}
function toRad(value) {
    return value * Math.PI / 180;
}
var c = u = 0;
db.getCollection("relevant_points2").find().limit(1).forEach(function (data) {
    if (!(++c % 100)) {
        print(c + "/" + u);
    }
    var date = data.date.toISOString().substr(0, 10);
    var hours = data.date.toISOString().substr(11, 2);
    var minutes = data.date.toISOString().substr(14, 2);
    var minDistance = 9999;
    var confidence = 0;
    var fireDataId = null;
    db.getCollection("modis_c6_usa_7d").find({acq_date: date}).forEach(function (fireData) {
        var distance = calcCrow(data.latitude, data.longitude, fireData.latitude, fireData.longitude);
        if (distance < minDistance) {
            minDistance = distance;
            confidence = fireData.confidence;
            fireDataId = fireData._id;
        }
    });
    if (minDistance <= 80) {
        u++;
        db.getCollection("relevant_points2").updateOne({ _id: data._id }, { $set: { confidence }});
        db.getCollection("modis_c6_usa_7d").updateOne({ _id: fireDataId }, { $inc: { assigned_to: 1 }});
    }
});