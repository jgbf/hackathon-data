/**
 * @param params
 * @param segments
 * @param distance
 * @returns []
 */
function getNewPoligionCoords(params, segments = 32, distance = 10) {
    var result = [];
    var steps = 360 / 32;

    if (!Array.isArray(params[0])) {
        params[0] = params[0] / 110.574;
        params[1] = params[0] / 111.320 * Math.cos(params[0] * (Math.PI / 180));

        for (var i = 0; i < segments; i++) {
            result.push(
                [
                    distance * Math.sin((steps * i) * (Math.PI / 180)) + params[0],
                    distance * Math.cos((steps * i) * (Math.PI / 180)) + params[1],
                ]
            );
        }
    }

    return result;
}

/**
 * @param params
 * @param distance
 * @param segments
 * @returns {[]}
 */
function getPoligionCoords(params, distance, segments) {
    var locs = [];
    var a = 360 / segments;

    if (!Array.isArray(params[0])) {
        params[0] = params[0] * Math.PI / 180.0;
        params[1] = params[1] * Math.PI / 180.0;
        distance = distance * 1.60934 / 3956;
        for (var x = 0; x <= 32; x++) {
            var lon;
            var tc = (a * x / 90) * Math.PI / 2;
            var lat = Math.asin(
                Math.sin(params[0]) * Math.cos(distance) + Math.cos(params[0]) * Math.sin(distance) * Math.cos(tc)
            );

            if (Math.cos(params[0]) === 0) {
                lon = params[1];
            } else {
                lon = ((params[1] - Math.asin(Math.sin(tc) * Math.sin(distance) / Math.cos(params[0])) + Math.PI) % (2 * Math.PI)) - Math.PI;
            }

            lat = 180.0 * lat / Math.PI;
            lon = 180.0 * lon / Math.PI;
            locs.push([lat, lon]);
        }
    } else {
        var p1 = 0;
        var p2 = 0;
        params.forEach(function(val) {
            p1 += val[0];
            p2 += val[1];
        });
        p1 = p1 / params.length;
        p2 = p2 / params.length;

        params.forEach(function (val, ) {
            var e = Math.sqrt(Math.pow(val[0] - p1, 2) + Math.pow(val[1] - p2, 2));
            var szog = Math.acos((val[0] - p1) / e) * 180 / Math.PI;
            e = e + distance;
        })
    }

    return locs;
}

var feed = [
    [ 46.39983220267797, 20.144999999999992 ],
    [ 46.39704198867416, 20.104136115770753 ],
    [ 46.38877940981924, 20.064842588343545 ],
    [ 46.35731597239616, 19.99688829934056 ],
    [ 46.31026142411542, 19.951482584852087 ],
    [ 46.199412085977634, 19.951482584852087 ],
    [ 46.152492773330735, 19.99688829934056 ],
    [ 46.11016779732204, 20.144999999999992 ],
    [ 46.199412085977634, 20.3385174151479 ],
]
// console.log(getPoligionCoords([46.255, 20.145], 6.21371, 32));
console.log(getPoligionCoords(feed, 6.21371, 32));
// console.log(getNewPoligionCoords([46.255, 20.145]));

