const https = require('https')

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://root:gtsMTdPp3Q@34.123.34.16:27017/admin";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;

    var dbo = db.db("hackathon");
    dbo.collection("relevant_points").find({}).toArray(function(err, result) {
        if (err) throw err;

        var i, j, temparray, chunk = 100, x=0;
        for (i = 0, j = result.length; i < j; i += chunk) {
            temparray = result.slice(i, i + chunk);
            var stick = '';
            var ids = [];
            temparray.forEach(function (item) {
                stick += item.latitude.trim() + ',' + item.longitude.trim() + '|';
                ids.push(item._id);
            });

            var option = {
                hostname: 'api.opentopodata.org',
                port: 443,
                path: '/v1/srtm90m?locations=' + stick,
                method: 'GET',
                _items: ids
            }

            setTimeout(function (option) {
                var ids = option._items
                delete option._items

                var req = https.request(option, res => {
                    var body = '';
                    res.on('data', data => {
                        body += data;
                    })

                    res.on('end', function () {
                        var MongoClient = require('mongodb').MongoClient;
                        var url = "mongodb://root:gtsMTdPp3Q@34.123.34.16:27017/admin";

                        MongoClient.connect(url, function(err, db) {
                            if (err) throw err;

                            var entry = JSON.parse(body);

                            entry.results.forEach(function (elem, key) {
                                delete elem.location;
                                elem.external_id = ids[key];
                            });

                            var dbo = db.db("hackathon");
                            dbo.collection("topography").insertMany(entry.results);
                            db.close();
                        });
                    });
                })

                req.on('error', error => {
                    console.error(error)
                })

                req.end()
            },
                x * 1000, option)
            x++
        }

        db.close();
    });
});
