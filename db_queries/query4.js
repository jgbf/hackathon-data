/**
 * @param params
 * @param distance
 * @param segments
 * @returns {[]}
 */
function getPoligionCoords(params, distance = 6.21371, segments = 34) {
    var locs = [];
    var a = 360 / segments;

    if (!Array.isArray(params[0])) {
        console.log(params);
        params[0] = params[0] * Math.PI / 180.0;
        params[1] = params[1] * Math.PI / 180.0;

        for (var x = 0; x < segments; x++) {
            var result = convertGPS(params, distance, x * a);
            locs.push([result[0], result[1]]);
        }
    } else {
        var p1 = 0;
        var p2 = 0;
        params.forEach(function(val) {
            p1 += val[0];
            p2 += val[1];
        });
        p1 = p1 / params.length;
        p2 = p2 / params.length;

        params.forEach(function (val) {
            var e = calcCrow(val[0], val[1], p1, p2);
            var szog = Math.acos((val[0] - p1) / e) * 180 / Math.PI;

            console.log(szog)
            return
            e = e + distance;
            // p1 = p1 * Math.PI / 180.0;
            // p2 = p2 * Math.PI / 180.0;
            // console.log([p1, p2], e, szog)
            var s1 = p1 * Math.PI / 180;
            var s2 = p2 * Math.PI / 180;
            var result = convertGPS([s1, s2], e, szog);
            locs.push(result);
        })
    }

    return locs;
}

/**
 *
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @returns {number}
 */
function calcCrow(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

/**
 * @param value
 * @returns {number}
 */
function toRad(value) {
    return value * Math.PI / 180;
}

/**
 * @param params
 * @param distance
 * @param deg
 * @returns {number[]}
 */
function convertGPS(params, distance, deg) {
    distance = distance * 1.60934 / 3956;
    var tc = (deg / 90) * Math.PI / 2;
    var lat = Math.asin(
        Math.sin(params[0]) * Math.cos(distance) + Math.cos(params[0]) * Math.sin(distance) * Math.cos(tc)
    );
    var lon;

    if (Math.cos(params[0]) === 0) {
        lon = params[1];
    } else {
        lon = ((params[1] - Math.asin(Math.sin(tc) * Math.sin(distance) / Math.cos(params[0])) + Math.PI) % (2 * Math.PI)) - Math.PI;
    }

    lat = 180.0 * lat / Math.PI;
    lon = 180.0 * lon / Math.PI;
    return [lat, lon];
}

var feed = [
    [ 46.39983220267797, 20.144999999999992 ],
    [ 46.39704198867416, 20.104136115770753 ],
    [ 46.38877940981924, 20.064842588343545 ],
    [ 46.35731597239616, 19.99688829934056 ],
    [ 46.31026142411542, 19.951482584852087 ],
    [ 46.199412085977634, 19.951482584852087 ],
    [ 46.152492773330735, 19.99688829934056 ],
    [ 46.11016779732204, 20.144999999999992 ],
    [ 46.199412085977634, 20.3385174151479 ],
]
// console.log(getPoligionCoords([46.255, 20.145], 6.21371, 32));
console.log(getPoligionCoords(feed));
// console.log(calcCrow(46.256325, 20.122002, 46.248152, 20.108373)) //1.39km
// console.log(getNewPoligionCoords([46.255, 20.145]));


